package a1;
/**
 * @author Ho Hang Au Yeung
 * @since 2014-04-02
 * pick 2 cards randomly
 */
public class PickTwoCards {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Card card = new Card();
		int num1 = (int)(Math.random()*12+1);	
		card.setnum(num1);
		System.out.println(card.getnum()+" of "+card.getsuit());
		int num2 = (int)(Math.random()*12+1);
		card.setnum(num2);
		card.setsuit('h');
		System.out.println(card.getnum()+" of "+card.getsuit());
	}

}
