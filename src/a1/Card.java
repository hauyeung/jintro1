package a1;
/**
 * @author Ho Hang Au Yeung
 * @since 2014-04-02
 * card class - gets and set card value and suit
 */
public class Card {
	
	char spade = 's';
	int num = 0;
	/**
	 * get card value
	 * @return card value
	 */
	public int getnum()
	{
		return num;	
	}
	/**
	 * set card value
	 * @param x value of card
	 */
	public void setnum(int x)
	{
		num = x;
	}
	/**
	 * get suit value
	 * @return suit value
	 */
	public char getsuit()
	{
		return spade;
	}
	/**
	 * set card value
	 * @param x value of suit
	 */
	public void setsuit(char s)
	{
		spade = s;
	}


}
